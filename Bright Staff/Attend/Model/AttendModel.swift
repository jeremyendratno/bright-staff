//
//  AttendModel.swift
//  Bright Staff
//
//  Created by Jeremy Endratno on 1/4/22.
//

import Foundation

struct Attend: Decodable {
    var Message: Message
    var Data: [AttendData]
}

struct AttendData: Codable {
    var Name: String?
    var DateIn: String?
    var DateOut: String?
    var DistanceInDesc: String?
    var DistanceOutDesc: String?
    
    enum CodingKeys: String, CodingKey {
        case Name = "Name"
        case DateIn = "DateIn"
        case DateOut = "DateOut"
        case DistanceInDesc = "DistanceInDesc"
        case DistanceOutDesc = "DistanceOutDesc"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Name = try values.decodeIfPresent(String.self, forKey: .Name)
        DateIn = try values.decodeIfPresent(String.self, forKey: .DateIn)
        DateOut = try values.decodeIfPresent(String.self, forKey: .DateOut)
        DistanceInDesc = try values.decodeIfPresent(String.self, forKey: .DistanceInDesc)
        DistanceOutDesc = try values.decodeIfPresent(String.self, forKey: .DistanceOutDesc)
    }
}

